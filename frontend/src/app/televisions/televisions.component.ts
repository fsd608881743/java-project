import { Component } from '@angular/core';
import { CartService } from '../cart.service';


@Component({
  selector: 'app-televisions',
  templateUrl: './televisions.component.html',
  styleUrl: './televisions.component.css'
})
export class TelevisionsComponent {
  emailId:any;
  products: any;
  televisions: any;

  constructor(private cartService: CartService) {

    this.televisions = [
      {
        id: 117,
        name: 'INBOOK',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Infinix INBook X1 Intel Core i3 ',
        img: 'assets/Televisions/a1.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Televisions/a18.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Televisions/a3.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Televisions/a4.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/WashingMachines/a4.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Televisions/a5.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Televisions/a6.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Televisions/a7.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Televisions/a8.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Televisions/a9.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Televisions/a10.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Televisions/a11.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Televisions/a12.jpeg',
      },
    ];
  }
  addToCart(product: any) {
    this.cartService.addToCart(product);
  }
}


// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { UserService } from '../user.service';
// import { ToastrService } from 'ngx-toastr';

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrls: ['./register.component.css'],
// })
// export class RegisterComponent implements OnInit {

//   emp: any;
//   countries: any;
//   departments: any;
//   siteKey: string;

//   constructor(private router: Router, private service: UserService, private toastr: ToastrService) {
//     this.siteKey = '6LdaVagpAAAAAESiXy26lahakgLzzgVnjKi5prmf'

//     this.emp = {
//       empId: '',
//       userName: '',
//       salary: '',
//       gender: '',
//       dob: '',
//       country: '',
//       emailId: '',
//       password: '',
//     }
//   }

//   ngOnInit() {
//     this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
//     this.service.getAllDepartments().subscribe((data: any) => { this.departments = data; });
//   }

//   empRegister(regForm: any) {
    
//     const mobileNumber = regForm.Mobile.toString(); 
//     const mobilePattern = /^[6-9]\d{9}$/; 
//     if (!mobilePattern.test(mobileNumber)) {
//       this.toastr.error("Invalid mobile number", "", 
//       { timeOut: 3000,
//         progressBar: true,
//         progressAnimation: 'increasing', });
//       return; 
//     }
    

//     this.emp.userName = regForm.userName;
//     this.emp.gender = regForm.gender;
//     this.emp.dob = regForm.dob;
//     this.emp.country = regForm.country;
//     this.emp.emailId = regForm.emailId;
//     this.emp.password = regForm.password;

//     this.service.registerUser(this.emp).subscribe((data: any) => {
//       console.log(data);
//       this.toastr.success("Registration Success", "", 
//       { timeOut: 3000,
//         progressBar: true,
//         progressAnimation: 'increasing', });
//     }, error => {
//       this.toastr.error("Registration Failed", "", 
//       { timeOut: 3000,
//         progressBar: true,
//         progressAnimation: 'increasing', });
//     });
//   }

//   hasUppercase(value: string): boolean {
//     return /[A-Z]/.test(value);
//   }

//   hasLowercase(value: string): boolean {
//     return /[a-z]/.test(value);
//   }

//   hasNumber(value: string): boolean {
//     return /\d/.test(value);
//   }

//   hasSpecialChar(value: string): boolean {
//     return /[!@#$%^&*(),.?":{}|<>]/.test(value);
//   }

//   getMaxDate(): string {
//     const today = new Date();
//     const year = today.getFullYear();
//     let month: string | number = today.getMonth() + 1;
//     let day: string | number = today.getDate();

//     // Ensure month and day are two digits
//     month = (month < 10) ? '0' + month : month;
//     day = (day < 10) ? '0' + day : day;

//     return `${year}-${month}-${day}`;
//   }
// }


import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {

  emp: any;
  countries: any;
  departments: any;
  siteKey: string;

  constructor(private router: Router, private service: UserService, private toastr: ToastrService) {
    this.siteKey = '6LdaVagpAAAAAESiXy26lahakgLzzgVnjKi5prmf'

    this.emp = {
      empId: '',
      userName: '',
      salary: '',
      gender: '',
      dob: '',
      country: '',
      emailId: '',
      password: '',
    }
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
    this.service.getAllDepartments().subscribe((data: any) => { this.departments = data; });
  }

  empRegister(regForm: any) {
    const mobileNumber = regForm.Mobile.toString(); 
    const mobilePattern = /^[6-9]\d{9}$/; 
    if (!mobilePattern.test(mobileNumber)) {
      this.toastr.error("Invalid mobile number", "", 
      { timeOut: 3000,
        progressBar: true,
        progressAnimation: 'increasing', });
      return; 
    }
    
    this.emp.userName = regForm.userName;
    this.emp.gender = regForm.gender;
    this.emp.dob = regForm.dob;
    this.emp.country = regForm.country;
    this.emp.emailId = regForm.emailId;
    this.emp.password = regForm.password;

    this.service.registerUser(this.emp).subscribe((data: any) => {
      console.log(data);
      this.toastr.success("Registration Success", "", 
      { timeOut: 3000,
        progressBar: true,
        progressAnimation: 'increasing', });
      this.router.navigate(['/login']);
    }, error => {
      this.toastr.error("Registration Failed", "", 
      { timeOut: 3000,
        progressBar: true,
        progressAnimation: 'increasing', });
    });
  }

  hasUppercase(value: string): boolean {
    return /[A-Z]/.test(value);
  }

  hasLowercase(value: string): boolean {
    return /[a-z]/.test(value);
  }

  hasNumber(value: string): boolean {
    return /\d/.test(value);
  }

  hasSpecialChar(value: string): boolean {
    return /[!@#$%^&*(),.?":{}|<>]/.test(value);
  }

  getMaxDate(): string {
    const today = new Date();
    const year = today.getFullYear();
    let month: string | number = today.getMonth() + 1;
    let day: string | number = today.getDate();

    month = (month < 10) ? '0' + month : month;
    day = (day < 10) ? '0' + day : day;

    return `${year}-${month}-${day}`;
  }
}

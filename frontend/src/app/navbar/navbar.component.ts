// import { Component, OnInit } from '@angular/core';
// import { CartService } from '../cart.service';

// @Component({
//   selector: 'app-navbar',
//   templateUrl: './navbar.component.html',
//   styleUrl: './navbar.component.css'
// })
// export class NavbarComponent implements  OnInit{
//   cartItemCount: number = 0; 
  
//   constructor(private cartService: CartService) {}

//   ngOnInit(): void {
//     this.cartService.getCartItems().subscribe(items => {
//       this.cartItemCount = items.length;
//     });
//   }
// }


// import { Component, OnInit } from '@angular/core';
// import { CartService } from '../cart.service';

// @Component({
//   selector: 'app-navbar',
//   templateUrl: './navbar.component.html',
//   styleUrls: ['./navbar.component.css']
// })
// export class NavbarComponent implements OnInit {
//   cartItemCount: number = 0; 
//   isLoggedIn: boolean = false; // Simulating authentication
  
//   constructor(private cartService: CartService) {}

//   ngOnInit(): void {
//     this.cartService.getCartItems().subscribe(items => {
//       this.cartItemCount = items.length;
//     });
//   }

//   login(): void {
//     // Simulate login logic
//     this.isLoggedIn = true;
//   }

//   logout(): void {
//     // Simulate logout logic
//     this.isLoggedIn = false;
//   }
// }


import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  cartItemCount: number = 0; 
  isLoggedIn: boolean = false; 
  
  constructor(private cartService: CartService) {}

  ngOnInit(): void {
    this.cartService.getCartItems().subscribe(items => {
      this.cartItemCount = items.length;
    });
  }

  login(): void {
    this.isLoggedIn = true;
  }

  logout(): void {
    this.isLoggedIn = false;
  }
}

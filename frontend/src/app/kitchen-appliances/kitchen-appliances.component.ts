import { Component } from '@angular/core';
import { CartService } from '../cart.service';


@Component({
  selector: 'app-kitchen-appliances',
  templateUrl: './kitchen-appliances.component.html',
  styleUrl: './kitchen-appliances.component.css'
})
export class KitchenAppliancesComponent {
  emailId:any;
  products: any;
  kitchenappliances: any;

  constructor(private cartService: CartService) {

    this.kitchenappliances = [
      {
        id: 117,
        name: 'INBOOK',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Infinix INBook X1 Intel Core i3 ',
        img: 'assets/KitchenAppliances/a1.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/KitchenAppliances/a2.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/KitchenAppliances/a3.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/KitchenAppliances/a4.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/KitchenAppliances/a4.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/KitchenAppliances/a5.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/KitchenAppliances/a6.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/KitchenAppliances/a7.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/KitchenAppliances/a8.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/KitchenAppliances/a9.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/KitchenAppliances/a10.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/KitchenAppliances/a11.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/KitchenAppliances/a12.jpeg',
      },
    ];
  }
  addToCart(product: any) {
    this.cartService.addToCart(product);
  }
}

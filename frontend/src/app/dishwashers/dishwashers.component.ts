import { Component } from '@angular/core';
import { CartService } from '../cart.service';


@Component({
  selector: 'app-dishwashers',
  templateUrl: './dishwashers.component.html',
  styleUrl: './dishwashers.component.css'
})
export class DishwashersComponent {
  emailId:any;
  products: any;
  dishwashers: any;

  constructor(private cartService: CartService) {

    this.dishwashers = [
      {
        id: 117,
        name: 'INBOOK',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Infinix INBook X1 Intel Core i3',
        img: 'assets/Dishwashers/a1.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Dishwashers/a2.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Dishwashers/a3.jpeg'
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Dishwashers/a4.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Dishwashers/a5.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Dishwashers/a6.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Dishwashers/a7.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Dishwashers/a8.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Dishwashers/a9.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Dishwashers/a10.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Dishwashers/a11.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Dishwashers/a12.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Dishwashers/a13.jpeg',
      },
    ];
  }
  addToCart(product: any) {
    this.cartService.addToCart(product);
  } 
}

import { Component } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-laptop',
  templateUrl: './laptop.component.html',
  styleUrl: './laptop.component.css',
})
export class LaptopComponent {
  emailId:any;
  products: any;
  laptops: any;
  constructor(private cartService: CartService ) {

    this.laptops = [
      {
        id: 117,
        name: 'INBOOK',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Infinix Inbook Y2 Plus Intel Core i3 11th Gen 1115G4',
        img: 'assets/Laptops/a1.jpeg',
      },
      {
        id: 117,
        name: 'SAMSUNG',
        price: 67999.0,
        originalPrice: 70999.0,
        description: 'SAMSUNG Galaxy Book 2 Intel Core i5 12th Gen 1235U ',
        img: 'assets/Laptops/a2.jpeg',
      },
      {
        id: 117,
        name: 'LENOVO',
        price: 47999.0,
        originalPrice: 57999.0,
        description: 'Lenovo IdeaPad Slim 3 Intel Core i3 12th Gen 1215U',
        img: 'assets/Laptops/a3.jpeg',
      },
      {
        id: 117,
        name: 'MSI',
        price: 77999.0,
        originalPrice: 80999.0,
        description: 'MSI Pulse 17 Intel Core i7 13th Gen 13700H',
        img: 'assets/Laptops/a4.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'HP Pavilion Intel Core i5 12th Gen  (16 GB/1 TB SSD/Windows 11)',
        img: 'assets/Laptops/a5.jpeg',
      },
      {
        id: 117,
        name: 'MSI',
        price: 57999.0,
        originalPrice: 70999.0,
        description: '  Intel Core i7 13th Gen 13700H - (16 GB/1 TB SSD )',
        img: 'assets/Laptops/a6.jpeg',
      },
      {
        id: 117,
        name: 'Realme',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Slim 3 Intel Core i3 12th Gen 1215U   B13VFK-667IN Gaming Laptop ',
        img: 'assets/Laptops/a7.jpeg',
      },
      {
        id: 117,
        name: 'CHUWI',
        price: 57999.0,
        originalPrice: 70999.0,
        description: '  Core i3 10th Gen 1005G1 - (8 GB/512 GB SSD/Windows)',
        img: 'assets/Laptops/a8.jpeg',
      },
      {
        id: 117,
        name: 'INFINIX',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Infinix X2 Slim Intel Core i3 11th Gen 1115G4 - (8 GB/512 GB SSD)',
        img: 'assets/Laptops/a9.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Laptops/a10.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Hyzen 3 Dual Core AMD Ryzen3 3250 - (8 GB/512 GB SSD)',
        img: 'assets/Laptops/a11.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Laptops/a12.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Laptops/a13.jpeg',
      },
    ];
  }
  addToCart(product: any) {
    this.cartService.addToCart(product);
  }
}

// import { Injectable } from '@angular/core';
// import { BehaviorSubject } from 'rxjs';

// // Define an interface for cart items
// interface CartItem {
//   name: string;
//   price: number;
//   imgSrc: string;
//   description: string;
//   quantity: number;
// }

// @Injectable({
//   providedIn: 'root',
// })
// export class CartService {
//   private cartItems: CartItem[] = [];
//   private cartSubject = new BehaviorSubject<CartItem[]>([]);

//   constructor() {}

//   getCartItems() {
//     return this.cartSubject.asObservable();
//   }

//   addToCart(product: CartItem) {
//     const existingIndex = this.cartItems.findIndex(item => item.name === product.name);
//     if (existingIndex !== -1) {
//       // If the product already exists in the cart, increase its quantity
//       this.cartItems[existingIndex].quantity++;
//     } else {
//       // Otherwise, add it as a new item
//       product.quantity = 1;
//       this.cartItems.push(product);
//     }
//     this.cartSubject.next([...this.cartItems]);
//   }

//   removeItem(index: number) {
//     this.cartItems.splice(index, 1);
//     this.cartSubject.next([...this.cartItems]);
//   }

//   decreaseQuantity(index: number) {
//     if (this.cartItems[index].quantity > 1) {
//       this.cartItems[index].quantity--;
//     } else {
//       this.removeItem(index);
//     }
//     this.cartSubject.next([...this.cartItems]);
//   }

//   purchaseItem(index: number) {
//     this.removeItem(index); // Remove the item from the cart
//   }
// }


import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

interface CartItem {
  name: string;
  price: number;
  img: string;
  description: string;
  quantity: number;
}

@Injectable({
  providedIn: 'root',
})
export class CartService {
  private cartItems: CartItem[] = [];
  private cartSubject = new BehaviorSubject<CartItem[]>([]);

  constructor() {}

  getCartItems() {
    return this.cartSubject.asObservable();
  }

  addToCart(product: CartItem) {
    const existingIndex = this.cartItems.findIndex(item => item.name === product.name);
    if (existingIndex !== -1) {
      this.cartItems[existingIndex].quantity++;
    } else {
      product.quantity = 1;
      this.cartItems.push(product);
    }
    this.cartSubject.next([...this.cartItems]);
  }

  removeItem(index: number) {
    this.cartItems.splice(index, 1);
    this.cartSubject.next([...this.cartItems]);
  }

  decreaseQuantity(index: number) {
    if (this.cartItems[index].quantity > 1) {
      this.cartItems[index].quantity--;
    } else {
      this.removeItem(index);
    }
    this.cartSubject.next([...this.cartItems]);
  }

  purchaseItem(index: number) {
    this.removeItem(index); // Removes the item from the cart
  }
}

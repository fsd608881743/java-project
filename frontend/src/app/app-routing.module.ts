import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { AboutComponent } from './about/about.component';
import { CategoriesComponent } from './categories/categories.component';
 
import { CartComponent } from './cart/cart.component';
import { HomeComponent } from './home/home.component';
import { LaptopComponent } from './laptop/laptop.component';
import { MobileComponent } from './mobile/mobile.component';
import { RefrigeratorsComponent } from './refrigerators/refrigerators.component';
import { WashingMachinesComponent } from './washing-machines/washing-machines.component';
import { KitchenAppliancesComponent } from './kitchen-appliances/kitchen-appliances.component';
 
import { DishwashersComponent } from './dishwashers/dishwashers.component';
import { TelevisionsComponent } from './televisions/televisions.component';
import { GeysersHeatersComponent } from './geysers-heaters/geysers-heaters.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { PaymentComponent } from './payment/payment.component';
import { AccountdetailsComponent } from './accountdetails/accountdetails.component';
import { NewarrivalsComponent } from './newarrivals/newarrivals.component';
import { ContactusComponent } from './contactus/contactus.component';



 
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'register', component: RegisterComponent },
  {path: 'about', component:AboutComponent},
  {path: 'Allcategories',component:CategoriesComponent},
 {path:'laptops',component:LaptopComponent},
  {path:'cart',component:CartComponent},
  {path:'home',component:HomeComponent},
 {path:'mobile',component:MobileComponent},
{path:'refrigerators',component:RefrigeratorsComponent},
{path:'washing-machines',component:WashingMachinesComponent},
{path:'kitchen-appliances',component:KitchenAppliancesComponent},
{path:'dishwashers',component:DishwashersComponent},
{path:'televisons',component:TelevisionsComponent},
{path:'geysers-heaters',component:GeysersHeatersComponent},
{path:'forgotpassword', component:ForgotPasswordComponent},
{path:'',component:HomeComponent},
{path:'payment' , component:PaymentComponent},
{path:'profile' ,  component:AccountdetailsComponent},
{path:'newarrivals', component:NewarrivalsComponent},
{path:'contactus', component:ContactusComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

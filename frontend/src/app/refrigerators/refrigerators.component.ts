import { Component } from '@angular/core';
import { CartService } from '../cart.service';


@Component({
  selector: 'app-refrigerators',
  templateUrl: './refrigerators.component.html',
  styleUrl: './refrigerators.component.css'
})
export class RefrigeratorsComponent {
  emailId:any;
  products: any;
  refrigerators  : any;

  constructor(private cartService: CartService ) {

    this.refrigerators = [
      {
        id: 117,
        name: 'SAMSUNG',
        price: 37999.0,
        originalPrice: 40999.0,
        description: 'Frost Free Double Door 2 Star Convertible Refrigerator  ',
        img: 'assets/Refigerators/a1.jpeg',
      },
      {
        id: 117,
        name: 'SAMSUNG',
        price: 17999.0,
        originalPrice: 30999.0,
        description: 'Frost Free single Door 2 Star Convertible Refrigerator ',
        img: 'assets/Refigerators/a2.jpeg',
      },
      {
        id: 117,
        name: 'WHIRPOOL',
        price: 27999.0,
        originalPrice:37999.0,
        description: 'Direct Cool Single Door 5 Star Refrigerator with Base Drawer',
        img: 'assets/Refigerators/a3.jpeg',
      },
      {
        id: 117,
        name: 'WHIRLPOOL',
        price: 47999.0,
        originalPrice: 50999.0,
        description: 'Direct Cool Single Door 5 Star Refrigerator with Base Drawer',
        img: 'assets/Refigerators/a4.jpeg',
      },
      {
        id: 117,
        name: 'LG',
        price: 47999.0,
        originalPrice: 45999.0,
        description: 'LG 185 L Direct Cool Single Door 4 Star Refrigerator with Base ',
        img: 'assets/Refigerators/a5.jpeg',
      },
      {
        id: 117,
        name: 'LG',
        price: 67999.0,
        originalPrice: 70999.0,
        description: 'LG 185 L Direct Cool Single Door 4 Star Refrigerator with Base ',
        img: 'assets/Refigerators/a6.jpeg',
      },
      {
        id: 117,
        name: 'LG',
        price: 19999.0,
        originalPrice: 20999.0,
        description: 'LG 185 L Direct Cool  Double Door 4 Star Refrigerator with Base ',
        img: 'assets/Refigerators/a7.jpeg',
      },
      {
        id: 117,
        name: ' HAIER',
        price: 37999.0,
        originalPrice: 40999.0,
        description: 'Haier 190 L Direct Cool Single Door 4 Star Refrigerator',
        img: 'assets/Refigerators/a8.jpeg',
      },
      {
        id: 117,
        name: 'HAIER',
        price: 34999.0,
        originalPrice: 35999.0,
        description: 'Haier 190 L Direct Cool Single Door 4 Star Refrigerator',
        img: 'assets/Refigerators/a9.jpeg',
      },
      {
        id: 117,
        name: ' HAIER',
        price: 47999.0,
        originalPrice: 49999.0,
        description: 'Haier 190 L Direct Cool Double Door 4 Star Refrigerator',
        img: 'assets/Refigerators/a10.jpeg',
      },
      {
        id: 117,
        name: 'GODREJ',
        price: 17999.0,
        originalPrice: 20999.0,
        description: 'Godrej 183 L Direct Cool Single Door 2 Star  ',
        img: 'assets/Refigerators/a11.jpeg',
      },
      {
        id: 117,
        name: 'GODREJ',
        price: 15999.0,
        originalPrice: 17999.0,
        description: 'Godrej 183 L Direct Cool Single Door 2 Star Refrigerator ',
        img: 'assets/Refigerators/a12.jpeg',
      },
      {
        id: 117,
        name: 'GODREJ',
        price: 20999.0,
        originalPrice: 21999.0,
        description: ' Godrej 183 L Direct Cool Single Door 2 Star Refrigerator ',
        img: 'assets/Refigerators/a13.jpeg',
      },
    ];
  }
  addToCart(product: any) {
    this.cartService.addToCart(product);
  }
}
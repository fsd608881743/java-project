


import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ReCaptcha2Component } from 'ngx-captcha';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  emailId: any;
  password: any;
  employees: any;
  emp: any;
  siteKey: string;
  @Output() loggedIn: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('captchaElem') captchaElem!: ReCaptcha2Component;

  constructor(
    private service: UserService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.siteKey = '6LdaVagpAAAAAESiXy26lahakgLzzgVnjKi5prmf';
  }

  ngOnInit(): void {}

  async loginSubmit(loginForm: NgForm) {
    localStorage.setItem('emailId', loginForm.value.emailId);

    if (loginForm.value.emailId == 'HR' && loginForm.value.password == 'HR') {
      this.toastr.success('Login Success', '', {
        timeOut: 3000,
        positionClass: 'toast-top-full-width',
        animate: 'slideUp'
      } as any); 
      this.loggedIn.emit(true);
      this.router.navigate(['/home']);
    } else {
      if (this.captchaElem && this.captchaElem.getResponse()) {
        await this.service.userLogin(loginForm.value.emailId, loginForm.value.password).then((data: any) => {
          this.emp = data;
        });

        if (this.emp != null) {
          this.toastr.success('Login Success', '', {
            timeOut: 3000,
            progressBar: true,
            progressAnimation: 'increasing',
            animate: 'slideUp' 
          } as any); 
          this.loggedIn.emit(true);
          this.router.navigate(['/home']);
        } else {
          this.toastr.error('Invalid Credentials', '', {
            timeOut: 3000,
            progressBar: true,
            progressAnimation: 'increasing',
          });
        }
      } else {
        this.toastr.error('Please complete the captcha', '', {
          timeOut: 3000,
          progressBar: true,
          progressAnimation: 'increasing',
        });
      }
    }
  }
}

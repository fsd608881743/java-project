import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';


@Component({
  selector: 'app-accountdetails',
  templateUrl: './accountdetails.component.html',
  styleUrl: './accountdetails.component.css'
})
export class AccountdetailsComponent implements OnInit {
  user: any; 

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.user = this.userService.getUserDetails();
  }
}
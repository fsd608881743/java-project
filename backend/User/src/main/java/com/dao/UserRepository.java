package com.dao;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.User; // Changed from Employee to User

@Repository
public interface UserRepository extends JpaRepository<User, Integer> { 

    @Query("from User u where u.userName = :userName") 
    User findByName(@Param("userName") String userName); 
//
//    @Query("from User u where u.emailId = :emailId and u.password = :password") 
//    User userLogin(@Param("emailId") String emailId, @Param("password") String password); 

    @Query("from User u  where u.emailId = :uEmailId and password = :upassword")
	User findByEmailAndPassword(@Param("uEmailId") String emailId,@Param("upassword") String password);

	 @Query("from User u where u.emailId = :uEmailId and u.password = :upassword")
	 User userLogin(@Param("uEmailId") String userEmailId, @Param("upassword") String userPassword);
	
	// forgot password code
	 @Query("from User u where u.emailId = :uemailId")
		User findByEmail(@Param("uemailId") String uemailId);
	 
}

import { Component } from '@angular/core';
import { CartService } from '../cart.service';


@Component({
  selector: 'app-geysers-heaters',
  templateUrl: './geysers-heaters.component.html',
  styleUrl: './geysers-heaters.component.css'
})
export class GeysersHeatersComponent {
  emailId:any;
  products: any;
  geysersheaters: any;

  constructor(private cartService: CartService ) {

    this.geysersheaters = [
      {
        id: 117,
        name: 'INBOOK',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Infinix INBook X1 Intel Core i3 ',
        img: 'assets/Geysers&Heaters/a1.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Geysers&Heaters/a2.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Geysers&Heaters/a3.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Geysers&Heaters/a4.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Geysers&Heaters/a4.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Geysers&Heaters/a5.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Geysers&Heaters/a6.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Geysers&Heaters/a7.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Geysers&Heaters/a8.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Geysers&Heaters/a9.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Geysers&Heaters/a10.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Geysers&Heaters/a11.jpeg',
      },
      {
        id: 117,
        name: 'HP',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/Geysers&Heaters/a12.jpeg',
      },
    ];
  }
  addToCart(product: any) {
    this.cartService.addToCart(product);
  }  
}

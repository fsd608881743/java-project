package com.model;


import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User { 

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private int userId; 
    private String userName; 
    private String gender;
    private String dob; 
    private String country;
    private String emailId;
    private String password; 
    
  
   
    public User() {
    }

    public User(int userId, String userName, String gender, String dob, String country, String emailId,
                String password) { 
        this.userId = userId;
        this.userName = userName;
        this.gender = gender;
        this.dob = dob;
        this.country = country;
        this.emailId = emailId;
        this.password = password;
    }

    public int getUserId() { 
        return userId;
    }

    public void setUserId(int userId) { 
        this.userId = userId;
    }

    public String getUserName() { 
        return userName;
    }

    public void setUserName(String userName) { 
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() { 
        return dob;
    }

    public void setDob(String dob) { 
        this.dob = dob;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public String getConfirmPassword() {
//        return confirmPassword;
//    }
//
//    public void setConfirmPassword(String confirmPassword) {
//        this.confirmPassword = confirmPassword;
//    }

   

    @Override
    public String toString() {
        return "User [userId=" + userId + ", userName=" + userName + ", gender=" + gender
                + "]";
    }
}
import { Component } from '@angular/core';
import { CartService } from '../cart.service';


@Component({
  selector: 'app-washing-machines',
  templateUrl: './washing-machines.component.html',
  styleUrl: './washing-machines.component.css'
})
export class WashingMachinesComponent {
  emailId:any;
  products: any;
  washingmachines: any;

  constructor(private cartService: CartService) {

    this.washingmachines = [
      {
        id: 117,
        name: 'SAMSUNG',
        price: 37999.0,
        originalPrice: 40999.0,
        description: '6.5 kg 5 star, Air Turbo Drying Semi Automatic   Washing Machine ',
        img: 'assets/WashingMachines/a1.jpeg',
      },
      {
        id: 117,
        name: 'SAMSUNG',
        price: 17999.0,
        originalPrice: 19999.0,
        description: '9.5 kg 5 star, Air Turbo Drying Fully Automatic   Washing Machine',
        img: 'assets/WashingMachines/a18.jpeg',
      },
      {
        id: 117,
        name: 'SAMSUNG',
        price: 27999.0,
        originalPrice: 30999.0,
        description: '6.5 kg 5 star, Air Turbo Drying Top  Automatic   Washing Machine',
        img: 'assets/WashingMachines/a3.jpeg',
      },
      {
        id: 117,
        name: 'SAMSUNG',
        price: 57999.0,
        originalPrice: 70999.0,
        description: '7 kg Diamond Drum Fully Automatic   Washing Machine Silver',
        img: 'assets/WashingMachines/a4.jpeg',
      },
      {
        id: 117,
        name: 'PANASONIC',
        price: 27999.0,
        originalPrice:40999.0,
        description: '7 kg 12 Wash Programs Active Foam Wash Fully Automatic  ',
        img: 'assets/WashingMachines/a4.jpeg',
      },
      {
        id: 117,
        name: 'PANASONIC',
        price: 49999.0,
        originalPrice: 50999.0,
        description: '9 kg 12 Wash Programs Active Foam Wash Fully Automatic',
        img: 'assets/WashingMachines/a5.jpeg',
      },
      {
        id: 117,
        name: 'THOMSON',
        price: 37999.0,
        originalPrice: 40999.0,
        description: '7.5 kg 5 Star Aqua Magic   Automatic   Washing Machine Black',
        img: 'assets/WashingMachines/a6.jpeg',
      },
      {
        id: 117,
        name: 'HAIER',
        price: 17999.0,
        originalPrice: 20999.0,
        description: '4.5 kg 5 Star Aqua Magic Semi Automatic  Washing Machine Black',
        img: 'assets/WashingMachines/a7.jpeg',
      },
      {
        id: 117,
        name: 'HAIER',
        price: 27999.0,
        originalPrice: 30999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/WashingMachines/a8.jpeg',
      },
      {
        id: 117,
        name: 'HAIER',
        price: 37999.0,
        originalPrice: 40999.0,
        description: 'Standard EMI starting from ₹561/month',
        img: 'assets/WashingMachines/a9.jpeg',
      },
      {
        id: 117,
        name: 'LG',
        price: 40999.0,
        originalPrice: 46999.0,
        description: '7 kg 5 Star with Wind Jet Dry,   Scrubber and Rust Free Plastic',
        img: 'assets/WashingMachines/a10.jpeg',
      },
      {
        id: 117,
        name: 'LG',
        price: 9999.0,
        originalPrice: 10999.0,
        description: ' 7 kg 5 Star with Steam, Inverter Drive Technology, Touch Panel',
        img: 'assets/WashingMachines/a11.jpeg',
      },
      {
        id: 117,
        name: 'LG',
        price: 13999.0,
        originalPrice: 15999.0,
        description: ' 7 kg 5 Star with Steam, Inverter   Drive Technology, Touch Panel',
        img: 'assets/WashingMachines/a12.jpeg',
      },
    ];
  }
  addToCart(product: any) {
    this.cartService.addToCart(product);
  }
}


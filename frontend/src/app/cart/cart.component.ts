// import { Component, OnInit } from "@angular/core";
// import { CartService } from "../cart.service";

// interface CartItem {
//   name: string;
//   price: number;
//   description: string;
//   imgSrc: string;
//   quantity: number;
// }

// @Component({
//   selector: 'app-cart',
//   templateUrl: './cart.component.html',
//   styleUrls: ['./cart.component.css']
// })
// export class CartComponent implements OnInit {
//   cartItems: CartItem[] = [];
//   totalAmount = 0;

//   constructor(private cartService: CartService) {}

//   ngOnInit() {
//     this.cartService.getCartItems().subscribe(items => {
//       this.cartItems = items;
//       this.calculateTotalAmount();
//     });
//   }

//   calculateTotalAmount() {
//     this.totalAmount = this.cartItems.reduce((total, item) => total + item.price * item.quantity, 0);
//   }

//   removeFromCart(index: number) {
//     this.cartService.removeItem(index);
//     this.calculateTotalAmount();
//   }

//   updateQuantity(index: number, change: number) {
//     if (this.cartItems[index].quantity === 1 && change === -1) {
//       this.removeFromCart(index);
//     } else {
//       this.cartItems[index].quantity += change;
//       this.calculateTotalAmount();
//     }
//   }

//   redirectToPaymentGateway() {
//     // Replace 'payment-gateway-url' with the actual URL of your payment gateway
//     window.location.href = 'payment-gateway-url';
//   }
//   purchase() {
//     this.router.navigate(['payment', { amount: this.total }]);
//     this.service.setCartItems();
//     this.total = 0;
//   }
// }

import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { CartService } from "../cart.service";

interface CartItem {
  name: string;
  price: number;
  description: string;
  img: string;
  quantity: number;
}

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartItems: CartItem[] = [];
  totalAmount = 0;

  constructor(private cartService: CartService, private router: Router) {}

  ngOnInit() {
    this.cartService.getCartItems().subscribe(items => {
      this.cartItems = items;
      this.calculateTotalAmount();
    });
  }

  calculateTotalAmount() {
    this.totalAmount = this.cartItems.reduce((total, item) => total + item.price * item.quantity, 0);
  }

  removeFromCart(index: number) {
    this.cartService.removeItem(index);
    this.calculateTotalAmount();
  }

  decreaseQuantity(index: number) {
    this.cartService.decreaseQuantity(index);
    this.calculateTotalAmount();
  }

  purchase() {
    this.router.navigate(['/payment']);
  }

  updateQuantity(index: number, change: number) {
    if (change === 1) {
      this.cartItems[index].quantity++;
    } else {
      if (this.cartItems[index].quantity > 1) {
        this.cartItems[index].quantity--;
      } else {
        this.removeFromCart(index);
      }
    }
    this.calculateTotalAmount();
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { NgxCaptchaModule } from 'ngx-captcha';
import { RecaptchaModule } from 'ng-recaptcha';
import { AboutComponent } from './about/about.component';
import { FooterComponent } from './footer/footer.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoriesComponent } from './categories/categories.component';
 
import { CartComponent } from './cart/cart.component';
import { HomeComponent } from './home/home.component';
import { LaptopComponent } from './laptop/laptop.component';
import { MobileComponent } from './mobile/mobile.component';
import { RefrigeratorsComponent } from './refrigerators/refrigerators.component';
import { WashingMachinesComponent } from './washing-machines/washing-machines.component';
import { DishwashersComponent } from './dishwashers/dishwashers.component';
import { KitchenAppliancesComponent } from './kitchen-appliances/kitchen-appliances.component';
import { GeysersHeatersComponent } from './geysers-heaters/geysers-heaters.component';
import { TelevisionsComponent } from './televisions/televisions.component';
import{CarouselModule}from 'ngx-bootstrap/carousel';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SearchproductComponent } from './searchproduct/searchproduct.component'
import { PaymentComponent } from './payment/payment.component';
import { AccountdetailsComponent } from './accountdetails/accountdetails.component';
import { NewarrivalsComponent } from './newarrivals/newarrivals.component';
import { ContactusComponent } from './contactus/contactus.component';
  
 

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    AboutComponent,
    FooterComponent,
    CategoriesComponent,
   
    CartComponent,
    HomeComponent,
    LaptopComponent,
    MobileComponent,
    RefrigeratorsComponent,
    WashingMachinesComponent,
    DishwashersComponent,
    KitchenAppliancesComponent,
    GeysersHeatersComponent,
    TelevisionsComponent,
    ForgotPasswordComponent,
    SearchproductComponent,
    PaymentComponent,
    AccountdetailsComponent,
    NewarrivalsComponent,
    ContactusComponent,
    
 

   
   
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    NgxCaptchaModule,
    RecaptchaModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    CarouselModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

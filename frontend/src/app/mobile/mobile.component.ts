import { Component } from '@angular/core';
import { CartService } from '../cart.service';


@Component({
  selector: 'app-mobile',
  templateUrl: './mobile.component.html',
  styleUrl: './mobile.component.css',
})
export class MobileComponent {
  emailId:any;
  products: any;
  mobiles: any;

  constructor(private cartService: CartService) {

    this.mobiles = [
      {
        id: 117,
        name: 'Motorola',
        price: 17999.0,
        originalPrice: 20999.0,
        description: 'Motorola G34 5G (Ocean Green, 128 GB)  (8 GB RAM) ',
        img: 'assets/Mobiles/a1.jpeg',
      },
      {
        id: 117,
        name: 'SAMSUNG',
        price: 57999.0,
        originalPrice: 70999.0,
        description: '  Galaxy Z Fold5 (Phantom Black, 256 GB)  (12 GB RAM)',
        img: 'assets/Mobiles/a3.jpeg',
      },
      {
        id: 117,
        name: 'BLACKBERRY',
        price: 27999.0,
        originalPrice: 30999.0,
        description: 'BlackBerry Q5 (Red, 8 GB) (2 MEMORY GB RAM)',
        img: 'assets/Mobiles/a2.jpeg',
      },
      {
        id: 117,
        name: 'SAMSUNG',
        price: 57999.0,
        originalPrice: 70999.0,
        description: ' Galaxy Z Flip3 5G (Phantom Black, 128 GB)  (8 GB RAM)',
        img: 'assets/Mobiles/a4.jpeg',
      },
      {
        id: 117,
        name: 'NOTHING',
        price: 27999.0,
        originalPrice: 30999.0,
        description: 'Nothing Phone (1) (Black, 256 GB),Full HD Flexible OLED Display',
        img: 'assets/Mobiles/a5.jpeg',
      },
      {
        id: 117,
        name: 'REALME',
        price: 10999.0,
        originalPrice: 15999.0,
        description: 'realme C53 (Champion Gold, 128 GB)  (6 GB RAM)',
        img: 'assets/Mobiles/a6.jpeg',
      },
      {
        id: 117,
        name: 'APPLE',
        price: 117999.0,
        originalPrice: 140999.0,
        description: ' iPhone 13 Pro Max (Alpine Green, 1 TB)',
        img: 'assets/Mobiles/a7.jpeg',
      },
      {
        id: 117,
        name: 'APPLE',
        price: 57999.0,
        originalPrice: 70999.0,
        description: ' iPhone 12 Pro Max (Alpine Green, 1 TB)',
        img: 'assets/Mobiles/a8.jpeg',
      },
      {
        id: 117,
        name: 'APPLE',
        price: 57999.0,
        originalPrice: 70999.0,
        description: ' iPhone 11  PRO MAX (Alpine Green, 1 TB)',
        img: 'assets/Mobiles/a9.jpeg',
      },
      {
        id: 117,
        name: 'POCO',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'POCO X6 Pro 5G (Spectre Black, 256 GB)  (8 GB RAM)',
        img: 'assets/Mobiles/a10.jpeg',
      },
      {
        id: 117,
        name: 'REDMI',
        price: 57999.0,
        originalPrice: 70999.0,
        description: 'Redmi 13C   5G (Titanium White, 256 GB)  (12 GB RAM)',
        img: 'assets/Mobiles/a12.jpeg',
      },
      {
        id: 117,
        name: 'SAMSUNG ',
        price: 126999.0,
        originalPrice: 130999.0,
        description: ' Galaxy S24 Ultra 5G (Titanium Gray, 256 GB)  (12 GB RAM)',
        img: 'assets/Mobiles/a11.jpeg',
      },
      {
        id: 117,
        name: 'POCO ',
        price: 57999.0,
        originalPrice: 70999.0,
        description: ' POCO C61  (Titanium Black, 256 GB)  (12 GB RAM)',
        img: 'assets/Mobiles/a13.jpeg',
      },
    ];
  }
  addToCart(product: any) {
    this.cartService.addToCart(product);
  }
}
import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
})
export class ForgotPasswordComponent {
  generatedOtp: any;
  otpSent: boolean = false;
  openResetPassword: boolean = false;
  user: any;
  closeForgotForm: boolean = false;

  constructor(
    private service: UserService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.user = {
      fullName: '',
      mobile: '',
      emailId: '',
      password: '',
    };
  }

  forgotPassword(emailId: any) {
    console.log(emailId);
    this.service.getUserByEmail(emailId).subscribe((data: any) => {
      this.user = data;
    });

    this.service.sendOtpToEmail(emailId).subscribe((data: any) => {
      console.log(data);
      this.generatedOtp = data;
      this.otpSent = true;
      console.log('Otp Sent!!!..');
    });
  }

  otpSubmit(otp: any) {
    if (this.generatedOtp == otp) {
      this.closeForgotForm = true;
      this.openResetPassword = true;
    }
  }

  resetPassword(password: any) {
    this.user.password = password;
    this.service.updateUserPassword(this.user).subscribe((data: any) => {
      // console.log("After UpDate : "+data);
      // console.log("After UpDate : "+data.password);
    });

    this.toastr.success('Password Reset Successfull', 'Password Reset Status', {
      timeOut: 3000,
      progressBar: true,
      progressAnimation: 'increasing',
    });

    this.router.navigate(['login']);
  }
}